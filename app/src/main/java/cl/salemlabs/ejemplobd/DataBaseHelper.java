package cl.salemlabs.ejemplobd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by harce on 17-05-2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    public static final int VERSION_DB = 3;
    public static final String NAME_BD = "nombre.db";

    public static final String NOMBRE_TABLA_CONTACTO = "contacto";

    public static final String TC_KEY_ID = "id";
    public static final String TC_KEY_NOMBRE = "nombre";
    public static final String TC_KEY_TELEFONO = "telefono";
    public static final String TC_KEY_EMAIL = "email";
    public static final String TC_KEY_DIRECCION = "direccion";

    public static final String TABLA_USUARIOS =
            "create table " + NOMBRE_TABLA_CONTACTO +
            "( " + TC_KEY_ID + " integer primary key autoincrement," +
            TC_KEY_NOMBRE + " text, " +
            TC_KEY_TELEFONO + " text, " +
            TC_KEY_EMAIL + " text, " +
            TC_KEY_DIRECCION + " text) ";

    public DataBaseHelper(Context context) {
        super(context, NAME_BD, null, VERSION_DB);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NOMBRE_TABLA_CONTACTO);
        onCreate(db);
    }

    public boolean insertaContacto(String nombre, String telefono, String email, String direccion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TC_KEY_NOMBRE, nombre);
        contentValues.put(TC_KEY_TELEFONO, telefono);
        contentValues.put(TC_KEY_EMAIL, email);
        contentValues.put(TC_KEY_DIRECCION, direccion);
        db.insert(NOMBRE_TABLA_CONTACTO, null, contentValues);
        return true;
    }

    public ArrayList<Contacto> getContactos() {
        ArrayList<Contacto> values = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + NOMBRE_TABLA_CONTACTO, null);
        if (res.moveToFirst()) {
            do {
                Contacto aux = new Contacto();
                aux.setId(res.getString(0));
                aux.setNombre(res.getString(1));
                aux.setTelefono(res.getString(2));
                aux.setEmail(res.getString(3));
                aux.setDireccion(res.getString(4));

                values.add(aux);

            } while (res.moveToNext());
        }
        return values;
    }

    public ArrayList<Contacto> getContactosQuery() {
        ArrayList<Contacto> values = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] columnas = new String[]{TC_KEY_ID, TC_KEY_NOMBRE, TC_KEY_TELEFONO,
                TC_KEY_EMAIL, TC_KEY_DIRECCION};

        String where = "id = 1 and nombre = 'juan' ";
        //String [] valores = new String[]{"1", "juan"};

        Cursor res = db.query(TABLA_USUARIOS, columnas, where, null, null, null, null);
        if (res.moveToFirst()) {
            do {
                Contacto aux = new Contacto();
                aux.setId(res.getString(0));
                aux.setNombre(res.getString(1));
                aux.setTelefono(res.getString(2));
                aux.setEmail(res.getString(3));
                aux.setDireccion(res.getString(4));

                values.add(aux);

            } while (res.moveToNext());
        }
        return values;
    }
}
