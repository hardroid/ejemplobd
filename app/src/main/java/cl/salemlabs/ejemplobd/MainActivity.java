package cl.salemlabs.ejemplobd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnGuardar;
    private Button btnContar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarContacto();
            }
        });

        btnContar = (Button)findViewById(R.id.btnContar);
        btnContar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contarContactos();
            }
        });
    }

    private void contarContactos() {
        DataBaseHelper db = new DataBaseHelper(this);
        Toast.makeText(this, "Existen " + db.getContactos().size() + " contactos", Toast.LENGTH_LONG).show();
    }

    private void guardarContacto() {
        DataBaseHelper db = new DataBaseHelper(this);
        db.insertaContacto("Nombre Prueba", "2222-22-222", "harttyn.arce@gmail.com", "Avenida siempre viva 509");
    }
}
